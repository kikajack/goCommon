package util

import (
	"net"
	"net/http"
	"strings"
)

// GetLocalIP get eth0 local ip
func GetLocalIP() string {
	netCard := "eth0"
	inter, err := net.InterfaceByName(netCard)
	if err != nil {
		return "0.0.0.0"
	}
	addrs, err := inter.Addrs()
	if err != nil {
		return "0.0.0.0"
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return "0.0.0.0"
}

// GetPublicIP get eth0 public ip
func GetPublicIP() string {
	// 	resp, err := http.Get("http://myexternalip.com/raw")
	// 	if err != nil {
	// 		return "0.0.0.0"
	// 	}
	// 	defer resp.Body.Close()
	// 	cont, _ := ioutil.ReadAll(resp.Body)
	// 	return string(cont)
	conn, _ := net.Dial("udp", "8.8.8.8:80")
	defer conn.Close()
	localAddr := conn.LocalAddr().String()
	idx := strings.LastIndex(localAddr, ":")
	return localAddr[0:idx]
}

// GetClientIP 返回远程客户端的 IP，如 192.168.1.1
func GetClientIP(req *http.Request) string {
	remoteAddr := req.RemoteAddr
	if ip := req.Header.Get("X-Real-IP"); ip != "" {
		remoteAddr = ip
	} else if ip = req.Header.Get("X-Forwarded-For"); ip != "" {
		remoteAddr = ip
	} else {
		remoteAddr, _, _ = net.SplitHostPort(remoteAddr)
	}

	if remoteAddr == "::1" {
		remoteAddr = "127.0.0.1"
	}

	return remoteAddr
}
