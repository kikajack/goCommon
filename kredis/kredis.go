package kredis

import (
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
)

var (
	rdb   = map[string]*redis.Client{}
	mutex sync.Mutex
)

type kRedisOpt struct {
	Network      string        // tcp/udp
	Addr         string        // 127.0.0.1:6379
	PoolSize     int           // conn pool size
	Auth         string        // auth password
	Database     int           // which database
	TimeOut      time.Duration // connect timeout
	PingInterval time.Duration // ping server seconds
	mustCreate   bool
}

type KRedis struct {
}

func New(opt kRedisOpt) (*KRedis, error) {
	mutex.Lock()
	defer mutex.Unlock()

	rdb := redis.NewClient(&redis.Options{
		Addr:     "132.232.7.147:6379",
		Password: "08370784lhfredish", // no password set
		DB:       0,                   // use default DB
	})

}
