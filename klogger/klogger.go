package klogger

import (
	"log"
	"net"
	"os"
	"path"
	"runtime"
	"strconv"
	"strings"
	"time"
)

const (
	logPath        = "/mydata/log/golang/"
	environmentKey = "ENVIRONMENT"
	environmentPrd = "PRD"
)

var (
	curEnv      string
	logFileName string
	kloggerIns  log.Logger
	ip          string
)

func pathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

// InitLogger init klogger
func InitLogger(serviceName string) {
	curDir := time.Now().Format("20060102") + "/"
	if serviceName == "" {
		_, file, _, ok := runtime.Caller(1)
		if !ok {
			panic("runtime.Caller err")
		}
		logFileName = path.Base(file) + ".log"
	} else {
		serviceName = transFileName(serviceName)
		logFileName = serviceName + ".log"
	}
	exists, err := pathExists(logPath + curDir)
	if err != nil {
		panic(err)
	}
	if !exists {
		err := os.MkdirAll(logPath+curDir, os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
	logFile, err := os.OpenFile(
		logPath+curDir+logFileName,
		os.O_CREATE|os.O_APPEND|os.O_RDWR,
		0744)
	if err != nil {
		panic(err)
	}

	ip = getLocalIP()
	log.SetFlags(0)
	log.SetOutput(logFile)
}

func transFileName(serviceName string) string {
	if serviceName == "" {
		return ""
	}
	serviceName = strings.ReplaceAll(serviceName, "\\", "#")
	serviceName = strings.ReplaceAll(serviceName, "/", "#")
	serviceName = strings.ReplaceAll(serviceName, ":", ";")
	serviceName = strings.ReplaceAll(serviceName, "\"", "$")
	serviceName = strings.ReplaceAll(serviceName, "*", "@")
	serviceName = strings.ReplaceAll(serviceName, "?", "!")
	serviceName = strings.ReplaceAll(serviceName, ">", ")")
	serviceName = strings.ReplaceAll(serviceName, "<", "(")
	serviceName = strings.ReplaceAll(serviceName, "|", "]")

	return serviceName
}

func writeLog(level string, msg string) {
	_, file, line, ok := runtime.Caller(2)
	if !ok {
		// send err
		return
	}
	content := ""
	content += time.Now().Format("2006-01-02 15:04:05")
	content += "\t\t" + level
	content += "\t\tIP:" + ip
	content += "\t\tFILE:" + file
	content += "\t\tLINE:" + strconv.Itoa(line)
	content += "\t\t" + msg
	log.Println(content)
}

// Info log level
func Info(msg string) {
	writeLog("INFO", msg)
}

// Debug log level, ignored in test environment
func Debug(msg string) {
	writeLog("DEBUG", msg)
}

// Error log level
func Error(msg string) {
	writeLog("ERROR", msg)
}

func getLocalIP() string {
	netCard := "eth0"
	inter, err := net.InterfaceByName(netCard)
	if err != nil {
		return "0.0.0.0"
	}
	addrs, err := inter.Addrs()
	if err != nil {
		return "0.0.0.0"
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return "0.0.0.0"
}
